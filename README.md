# COMP2300/6300 extension pack

This is just an [extension
pack](https://code.visualstudio.com/blogs/2017/03/07/extension-pack-roundup) of
extensions written by others---they deserve all the credit for their excellent
OSS work.

If you're enrolled in COMP2300/6300 at the ANU in semester 1 2018, this is
really useful. If you're not, then it's probably not so useful---better to
download the individual packages separately.

For more information on how to set up the COMP2300 software environment, visit
the [course web site](https://cs.anu.edu.au/courses/comp2300/resources/software-setup/).
