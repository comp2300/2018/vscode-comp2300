'use strict';
// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import { spawnSync } from 'child_process';
import * as path from 'path';
import { readFile, existsSync, writeFile, writeFileSync } from 'fs';
import * as os from 'os';
import { handle_samplot_cmd, PlotProvider } from './samplot';

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {
    let config = vscode.workspace.getConfiguration()
    let platform = os.platform();
    let env_home = (platform == 'win32') ? 'USERPROFILE' : 'HOME';

    config.update('cortex-debug.armToolchainPath',
        path.normalize(`\${env:${env_home}}/.platformio/packages/toolchain-gccarmnoneeabi/bin`));

    // st-util path on Windows has been changed in later version of PlatformIO
    let home = path.normalize(`${process.env[env_home]}`);
    let stlink_base = path.normalize(`${home}/.platformio/packages/tool-stlink`);
    let stlink_base_vscode = path.normalize(`\${env:${env_home}}/.platformio/packages/tool-stlink`);
    if (platform == 'win32') {
        config.update('cortex-debug.stutilPath',
            existsSync(path.join(stlink_base, "bin", "st-util.exe")) ?
                path.join(stlink_base_vscode, "bin", "st-util.exe") :
                path.join(stlink_base_vscode, "st-util.exe"));
    } else {
        config.update('cortex-debug.stutilPath',
            path.join(stlink_base_vscode, "bin", "st-util"));
    }
    
    config.update('platformio-ide.autoRebuildAutocompleteIndex', false);
    
    config.update('files.associations', {"*.S": "arm"});

    // register sample plot command
    let provider = new PlotProvider()
    context.subscriptions.push(
        vscode.workspace.registerTextDocumentContentProvider("samplot", provider),
        vscode.commands.registerCommand("comp2300.samplot", handle_samplot_cmd, {provider_inst: provider})
    )
    vscode.workspace.onDidChangeConfiguration((e: vscode.ConfigurationChangeEvent) => {
        provider.update()
    })

    vscode.workspace.findFiles("platformio.ini").then((uris) => {
        // config launch.json
        let folder = path.dirname(uris[0].fsPath);
        let launch_config_path = path.join(folder, ".vscode", "launch.json");

        if (!existsSync(launch_config_path)) {
            writeFileSync(launch_config_path, JSON.stringify({ "version": "0.2.0", "configurations": [] }));
        }

        readFile(launch_config_path, (err, data: Buffer) => {
            let launch_json: { version: string,
                               configurations: vscode.DebugConfiguration[],
                               components?: any[]} = JSON.parse(data.toString());
            let res = launch_json.configurations.find((config, idx, array) => {
                return  config.name == "ARM On-Chip Debug" &&
                        config.type == "cortex-debug" &&
                        config.servertype == "stutil";
            });
            if (res === undefined) {
                launch_json.configurations.push({
                    "type": "cortex-debug",
                    "request": "launch",
                    "name": "ARM On-Chip Debug",
                    "cwd": "${workspaceRoot}",
                    "executable": "./.pioenvs/disco_l476vg/firmware.elf",
                    "servertype": "stutil",
                    "device": "STM32L476vg",
                    "preLaunchTask": "PlatformIO: Upload",
                    "postLaunchCommands": [
                        "-break-insert main"
                    ]
                });
                launch_json.configurations = launch_json.configurations.reverse()
                writeFile(launch_config_path, JSON.stringify(launch_json, undefined, 4));
            }
        })

        // generate c_cpp_properties.json
        let python = path.normalize(
            (platform == "win32") ?
                path.join(home, ".platformio", "penv", "Scripts", "python.exe") :
                path.join(home, ".platformio", "penv", "bin", "python"));
        if (!existsSync(python)) {
            vscode.window.showErrorMessage(`Failed to find Python at ${python}`);
        }
        let extension_path = vscode.extensions.getExtension("anucecsit.comp2300-extension-pack").extensionPath;
        let res = spawnSync(python, [
            path.join(extension_path, "scripts", "cpathgen.py"), 
            "--assignment",
            folder]);
        if (res.status != 0) {
            let channel = vscode.window.createOutputChannel("Python");
            channel.append(res.output.join("\n"));
        }

        // set schema for assignment
        if (existsSync(path.join(folder, "statement-of-originality.yml"))) {
            config.update('yaml.schemas', {
                "https://cs.anu.edu.au/courses/comp2300/assets/schemas/statement-of-originality.json": "/statement-of-originality.yml"
            })
        }
    }, (err) => {
        console.log(err);
    })
}

// this method is called when your extension is deactivated
export function deactivate() {
}