import * as path from 'path';
import * as os from 'os';
import * as fs from 'fs';
import { spawn, spawnSync } from 'child_process';
import { GDB } from 'gdb-js'
import * as vscode from 'vscode'

type Sample = [number, number]
let channel = vscode.window.createOutputChannel("COMP2300")

export function handle_samplot_cmd(this) {
    vscode.window.showInputBox({
        value: '0',
        ignoreFocusOut: true,
        prompt: 'Starting sample index (0 is the very first sample)'
    }).then((idx_str) => {
        let idx_start = Number(idx_str)
        vscode.window.showInputBox({
            value: '220',
            ignoreFocusOut: true,
            prompt: 'Number of samples to plot'
        }).then((num_str) => {
            let num_samples = Number(num_str)
            if (num_samples > 0) {
                collect_samples(idx_start, num_samples).then((samples) => {
                    let provider: PlotProvider = this.provider_inst
                    provider.setSamples(samples)
                    provider.update()
                    vscode.commands.executeCommand('vscode.previewHtml', PlotProvider.PlotURI, vscode.ViewColumn.Two, 'Plot Samples')
                                    .then((success) => {}, (error) => {
                                        vscode.window.showErrorMessage(error)
                                    })
                }, (error) => {})
            }
        })
    }, (reason) => {})
}

async function collect_samples(idx_start: number, num_samples: number): Promise<Sample[]> {
    let config = vscode.workspace.getConfiguration()
    let platform = os.platform();
    let env_home = (platform == 'win32') ? 'USERPROFILE' : 'HOME';
    let home = path.normalize(`${process.env[env_home]}`);
    let pio_home = `${home}/.platformio`;
    let wsfolder = path.dirname((await vscode.workspace.findFiles("platformio.ini"))[0].fsPath);

    var pio_bin_name = "pio"
    var gdb_bin_name = "arm-none-eabi-gdb"
    var stutil_bin_name = "st-util"
    var penv_bin_dir_name = "bin"
    if (platform == "win32") {
        pio_bin_name = "pio.exe"
        gdb_bin_name = "arm-none-eabi-gdb.exe"
        stutil_bin_name = "st-util.exe"
        penv_bin_dir_name = "Scripts"
    }

    let pio_bin = path.normalize(`${pio_home}/penv/${penv_bin_dir_name}/${pio_bin_name}`);
    let stutil_bin = path.normalize(`${pio_home}/packages/tool-stlink/bin/${stutil_bin_name}`);
    let gdb_bin = path.normalize(`${pio_home}/packages/toolchain-gccarmnoneeabi/bin/${gdb_bin_name}`);
    // channel.show()

    // build & upload
    channel.append("Building and uploading...")
    let res = spawnSync(pio_bin, ['run', '-d', wsfolder, '--target', 'upload']);
    if (res.status != 0) {
        vscode.window.showErrorMessage("Build and upload failed, " +
                "see OUTPUT -> COMP2300 channel or run PlatformIO: Upload for more information");
        channel.appendLine("failed!")
        channel.append(res.stdout.toString());
        channel.append(res.stderr.toString());
        return new Promise<Sample[]>((resolve, reject) => reject());
    }
    channel.appendLine("done.");

    let elf_img = path.normalize(`${wsfolder}/.pioenvs/disco_l476vg/firmware.elf`);

    // launch st-util
    channel.appendLine("Launch st-util")
    let gdbport: string = config.get("comp2300.samplot.gdbport")
    let stutil_proc = spawn(stutil_bin, ['-p', gdbport]);
    stutil_proc.stdout.on("data", (data) => {
        channel.append(`[st-util] ${data}`);
    });
    stutil_proc.stderr.on("data", (data) => {
        channel.append(`[st-util] ${data}`);
    });
    stutil_proc.on("error", (err: Error) => {
        vscode.window.showErrorMessage(`An error occurred while launching st-util: ${err}`);
    })

    // launch GDB
    channel.appendLine("Launch GDB")
    let gdb_proc = spawn(gdb_bin, ['--nx', '--quiet', '--interpreter=mi2', elf_img]);
    gdb_proc.on("error", (err: Error) => {
        vscode.window.showErrorMessage(`An error occurred while launching GDB: ${err}`);
    })
    let gdb = new GDB(gdb_proc)
    try {
        await gdb.execMI(`-target-select extended-remote localhost:${gdbport}`);
        // await gdb.execMI(`-file-exec-and-symbols ${elf_img}`);
        await gdb.execMI(`-break-insert ${config.get("comp2300.samplot.function")}`);
        
        // capture samples
        let valid_bits: number = config.get("comp2300.samplot.valid-bits")
        let signed: boolean = config.get("comp2300.samplot.signed")
        channel.appendLine(`start capturing ${num_samples} samples...`);
        var samples: Sample[] = [];
        for (let t_idx = 0; t_idx < num_samples + idx_start; t_idx++) {
            await gdb.execMI("-exec-continue");
            let res = await gdb.execMI(`-data-list-register-values d ${config.get("comp2300.samplot.register")}`);
            let val = parseInt(res['register-values'][0].value)
            let sample = truncate(val, valid_bits, signed)
            if (t_idx >= idx_start)
                samples.push([t_idx, sample])
        }
    } catch (error) {
        vscode.window.showErrorMessage(`${error}`);
    }
    gdb.exit()
    stutil_proc.kill("SIGTERM")

    return new Promise<Sample[]>((resolve, reject) => resolve(samples))
}

function truncate(value: number, bits: number, signed: boolean): number {
    // mask the value with valid number of bits
    // convert to two's complement signed value
    let val = value & ((1 << bits) - 1)
    let signed_max = (1 << bits - 1) - 1
    if (signed && val > signed_max) {
        val = - ((1 << bits) - val)
    }
    return val
}

function reduce_samples(samples: Sample[]): Sample[] {
    // remove samples that fit on simple linear prediction y = a * x + b
    if ((samples.length) <= 2)
        return samples

    var cur_delta: number = samples[1][1] - samples[0][1]
    let reduced_samples = [samples[0]]
    for (let idx = 2; idx < samples.length; idx++) {
        let delta = samples[idx][1] - samples[idx - 1][1]
        if (delta != cur_delta && reduced_samples[-1] != samples[idx - 1]) {
            reduced_samples.push(samples[idx - 1])
        }
        cur_delta = delta
    }
    if (reduced_samples[reduce_samples.length - 1] != samples[samples.length - 1]) {
        reduced_samples.push(samples[samples.length - 1])
    }
    return reduced_samples
}

export class PlotProvider implements vscode.TextDocumentContentProvider {
    private _onDidChange = new vscode.EventEmitter<vscode.Uri>();
    public static PlotURI = vscode.Uri.parse("samplot://authority/plot")
    private plotjs_min_path: string;
    private samples: Sample[];

    constructor() {
        let extension_path = vscode.extensions.getExtension("anucecsit.comp2300-extension-pack").extensionPath;
        this.plotjs_min_path = path.join(extension_path, "node_modules/plotly.js/dist/plotly.min.js");
        if (!fs.existsSync(this.plotjs_min_path)) {
            vscode.window.showErrorMessage(`Failed to find plotly.js module at ${this.plotjs_min_path}`)
        }
        console.log(this.plotjs_min_path)
    }
    get onDidChange(): vscode.Event<vscode.Uri> { return this._onDidChange.event; }
    public setSamples(samples: Sample[]) { this.samples = samples }
    public update() { this._onDidChange.fire(PlotProvider.PlotURI); }
    public provideTextDocumentContent(uri: vscode.Uri): string {
        let config = vscode.workspace.getConfiguration()

        var samples = this.samples
        if (config.get("comp2300.samplot.reduce-samples"))
            samples = reduce_samples(samples);

        let x: number[] = [];
        let y: number[] = [];
        for (const [t_idx, sample] of samples) {
            x.push(t_idx)
            y.push(sample)
        }
        let width = config.get("comp2300.samplot.width")
        let height = config.get("comp2300.samplot.height")
        let html = `
        <!DOCTYPE HTML>
        <html>
            <head>
                <meta charset="utf-8">
                <script src="file://${this.plotjs_min_path}">
                </script>
            </head>
            <body>
                <script>
                (function() {
                    var d3 = Plotly.d3;
    
                    var WIDTH_IN_PERCENT_OF_PARENT = 100,
                        HEIGHT_IN_PERCENT_OF_PARENT = 95;
    
                    var gd3 = d3.select('body')
                        .append('div')
                        .style({
                            width: WIDTH_IN_PERCENT_OF_PARENT + '%',
                            'margin-left': (100 - WIDTH_IN_PERCENT_OF_PARENT) / 2 + '%',
    
                            height: HEIGHT_IN_PERCENT_OF_PARENT + 'vh',
                            'margin-top': (100 - HEIGHT_IN_PERCENT_OF_PARENT) / 2 + 'vh'
                        });
    
                    var gd = gd3.node();
                    let layout = {
                        yaxis: {
                            tickformat: "#X"
                        }
                    }
                    let trace = {
                        x: [${x}],
                        y: [${y}],
                        mode: "lines+markers",
                        type: "scattergl"
                    }
                    let fig = {
                        data: [trace],
                        layout: layout
                    }
                    Plotly.plot(gd, fig);
    
                    window.onresize = function() {
                        Plotly.Plots.resize(gd);
                    };
    
                    })();
                </script>
            </body>
        </html>
        `
        return html
    }
}
